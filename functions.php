<?php

/*-----------------------------------------------------------------------------------*/
/* Define Theme Constants
/*-----------------------------------------------------------------------------------*/

define( 'SD_FRAMEWORK', get_template_directory() .'/framework/' );
define( 'SD_FRAMEWORK_INC', get_template_directory() .'/framework/inc/' );
define( 'SD_FRAMEWORK_CSS', get_template_directory_uri() .'/framework/css/' );
define( 'SD_FRAMEWORK_JS', get_template_directory_uri() .'/framework/js/' );

// Define content width
if ( ! isset( $content_width ) ) $content_width = 1170;

/* ------------------------------------------------------------------------ */
/* Localization
/* ------------------------------------------------------------------------ */

$lang = SD_FRAMEWORK . '/lang';
load_theme_textdomain('sd-framework', $lang);

/* ------------------------------------------------------------------------ */
/* Inlcudes
/* ------------------------------------------------------------------------ */

// Enqueue JavaScripts & CSS
require_once( SD_FRAMEWORK_INC . 'enqueue.php');

// Theme Functions
require_once( SD_FRAMEWORK_INC . 'sd-theme-functions/sd-functions.php' );

// Include Widgets
require_once( SD_FRAMEWORK_INC . 'widgets/widgets.php' );

// Posts 2 Posts
require_once( SD_FRAMEWORK_INC . 'connection-types.php' );

// Visual Composer
if ( class_exists( 'Vc_Manager' ) ) {
	require_once( SD_FRAMEWORK_INC . 'vc/sd-vc-functions.php' );
}

// Redux Theme Options
if ( !class_exists( 'ReduxFramework' ) && file_exists( dirname( __FILE__ ) . '/admin/ReduxCore/framework.php' ) ) {
	require_once( dirname( __FILE__ ) . '/admin/ReduxCore/framework.php' );
}

if ( !isset( $redux_demo ) && file_exists( dirname( __FILE__ ) . '/admin/sd-admin-options/sd-admin-options.php' ) ) {
	require_once( dirname( __FILE__ ) . '/admin/sd-admin-options/sd-admin-options.php' );
}

/* Include Meta Box Script */
if ( !function_exists( 'sd_load_meta_box_plugin' ) ) {
	function sd_load_meta_box_plugin() {
		// Re-define meta box path and URL
		define( 'RWMB_URL', trailingslashit( get_template_directory_uri() . '/framework/inc/metabox' ) );
		define( 'RWMB_DIR', trailingslashit( get_template_directory() . '/framework/inc/metabox' ) );
		require_once RWMB_DIR . 'meta-box.php';
		include SD_FRAMEWORK_INC . 'metabox/the-meta-boxes.php';
	}
	add_action('init', 'sd_load_meta_box_plugin');
}

/* TGMA Automatic Plugin Activation */
require_once( SD_FRAMEWORK_INC . 'tgma/plugin-activation.php' );
require_once( SD_FRAMEWORK_INC . 'tgma/sd-tgma.php' );

// WP Advanced Search
// http://wpadvancedsearch.com
require_once( SD_FRAMEWORK_INC . 'wp-advanced-search/wpas.php' );
require_once( SD_FRAMEWORK_INC . 'wp-advanced-search/sd-wp-advanced-search.php' );


// CHANGE EXCERPT LENGTH FOR TESTIMONIALS POST TYPES
function testimonials_excerpt_length( $length ) {
	global $post;
	if ( $post->post_type == 'testimonials' )
		return 500;
}
add_filter( 'excerpt_length', 'testimonials_excerpt_length', 999 );

// Add custom assets
function cardone_univ_assets() {
	wp_enqueue_style( 'cardone-bootstrapvalidator', get_stylesheet_directory_uri() . '/assets/bootstrapvalidator/dist/css/bootstrapValidator.css' );
	wp_enqueue_script( 'cardone-bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js', array(), '3.3.5', true );
	wp_enqueue_script( 'cardone-is-tracking', 'https://sp204.infusionsoft.com/app/webTracking/getTrackingCode?trackingId=6314459ee1840d0e6e3fa0a3861222b7', array(), null, true );
	wp_enqueue_script( 'cardone-bootstrapvalidator', get_stylesheet_directory_uri() . '/assets/bootstrapvalidator/dist/js/bootstrapValidator.min.js', array(), '0.5.3', true );
	if ( ! is_page_template( 'live-training-page.php' ) ) {
		wp_enqueue_script( 'cardone-bootstrapvalidator-app', get_stylesheet_directory_uri() . '/assets/js/modal-form-validation.js', array(), null, true );
	}
	wp_enqueue_script( 'lightspeed-counter', 'http://a.lightspeedvt.com/v1/clients/grant_cardone/counter/init.cfm', array(), null, true );
	// wp_enqueue_script( 'cardone-js-cookie', get_stylesheet_directory_uri() . '/assets/js-cookie/src/js.cookie.js', array(), '2.0.3' );
}
add_action( 'wp_enqueue_scripts', 'cardone_univ_assets' );

function modify_courses_post_type() {
	$args = array(
		'public' => true,
		'label'  => 'Modules',
		'menu_icon' => 'dashicons-book-alt',
		'rewrite' => array( 'slug' => 'module' ),
		);
	register_post_type( 'courses', $args );
}
add_action( 'init', 'modify_courses_post_type' );

add_filter( 'livefyre_custom_comments_strings', 'comments_strings_filter' );
function comments_strings_filter() {
	$strings['signIn'] = "Sign In to comment";
	return $strings;
}

function remove_protected_from_post_title( $protected ) {
	return __( '%s' );
}
add_filter( 'protected_title_format', 'remove_protected_from_post_title' );

function code_request_live_page_form() {
	global $post;
	$label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
	$o = '<form action="' . esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ) . '" method="post"><h2>Ready to meetup?</h2><span>' . __( "Enter your training invite code below:" ) . '</span><label for="' . $label . '"><i class="fa fa-sign-in"></i> ' . __( "Join Training" ) . ' </label><input placeholder="Invite Code #" name="post_password" id="' . $label . '" type="password" size="20" maxlength="20" /><input type="submit" name="Submit" value="' . esc_attr__( "Enter" ) . '" />
</form>
';
return $o;
}
add_filter( 'the_password_form', 'code_request_live_page_form' );

// Allow hide for field labels on Gravity Forms
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

// Add bootstrap modal trigger to Selling 101 (Free Access) on middle menu at single module page.
function cu_add_modal_toggle( $atts, $item, $args ) {
    // Set the menu ID
	$menu_link = 906;
    // Conditionally match the ID and add the attribute and value
	if ($item->ID == $menu_link) {
		$atts['data-toggle'] = 'modal';
		$atts['data-target'] = '#free-cu-modal';
	}
    //Return the new attribute
	return $atts;
}
add_filter( 'nav_menu_link_attributes', 'cu_add_modal_toggle', 10, 3 );


// Add modals into footer so they can be ready to load any time from any page
function cu_hook_modals_to_footer() { ?>
<!-- Full CU Modal -->
<div class="modal fade" id="full-cu-modal" tabindex="-1" role="dialog" aria-labelledby="full-cu-modalLabel" style="display: none;">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<img src="<?= get_stylesheet_directory_uri() . '/images/modal-header-get-full-cu-access.png' ?>">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="full-cu-modalLabel">Complete the form below</h4>
			</div>
			<form id="full-cu-modalForm" name="insightly_web_to_contact" action="https://sp204.infusionsoft.com/app/form/process/9d5203482b19f085e7f68696dc007ed4" method="post">
				<div class="modal-body">
					<input name="inf_form_xid" type="hidden" value="9d5203482b19f085e7f68696dc007ed4" />
					<input name="inf_form_name" type="hidden" value="The Entire University" />
					<input name="infusionsoft_version" type="hidden" value="1.33.0.43" />

					<div class="form-group">
						<div class="col-md-6">
							<input class="form-control" id="inf_field_FirstName" name="inf_field_FirstName" placeholder="First Name*" type="text" />
						</div>
						<div class="col-md-6">
							<input class="form-control mobile-padding-top" id="inf_field_LastName" name="inf_field_LastName" placeholder="Last Name*" type="text" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-6">
							<input class="form-control field-top-margin" id="inf_field_Company" name="inf_field_Company" placeholder="Organization*" type="text" />
						</div>
						<div class="col-md-6">
							<select class="form-control field-top-margin" id="inf_field_JobTitle" name="inf_field_JobTitle">
								<option disabled selected> -- Select Role -- </option>
								<option>C-Level</option>
								<option>VP</option>
								<option>Manager</option>
								<option>Sales Person</option>
								<option>Other</option>
							</select>
						</div><input type="hidden" name="emails[0].Label" value="Work" />
					</div>

					<div class="form-group">
						<div class="col-md-6">
							<input class="form-control field-top-margin" id="inf_field_Email" class="insightly_email" name="inf_field_Email" placeholder="Email*" type="text" />
						</div><input type="hidden" name="phones[0].Label" value="Work" />
						<div class="col-md-6">
							<input class="form-control field-top-margin" id="inf_field_Phone1" class="insightly_phone" name="inf_field_Phone1" placeholder="Phone*" type="text" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-12">
							<textarea class="form-control field-top-margin" rows="6" id="inf_custom_Message" name="inf_custom_Message" placeholder="Background Information"></textarea>
							<span>*These fields are required and must be valid.</span>
							<input type="hidden" id="websites[0]_Value" name="websites[0].Value" value="http://cardoneuniversity.com/#motivation"/>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-default">Get FULL Access</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- Free CU Access Modal -->
<div class="modal fade" id="free-cu-modal" tabindex="-1" role="dialog" aria-labelledby="free-cu-modalLabel" style="display: none;">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<img src="<?= get_stylesheet_directory_uri() . '/images/' . (is_page(361) ? 'modal-free-access-homepage.png' : 'modal-header-getaccess.png') ?>">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="free-cu-modalLabel">Complete the form below</h4>
			</div>
			<form id="free-cu-modalForm" name="insightly_web_to_contact" action="https://sp204.infusionsoft.com/app/form/process/3e17c653e0980a5a54cc77c64b11938b" method="post">
				<div class="modal-body">
					<input name="inf_form_xid" type="hidden" value="3e17c653e0980a5a54cc77c64b11938b" />
					<input name="inf_form_name" type="hidden" value="CU A La Carte" />
					<input name="infusionsoft_version" type="hidden" value="1.33.0.43" />

					<div class="form-group">
						<div class="col-md-6">
							<input class="form-control" id="inf_field_FirstName" name="inf_field_FirstName" placeholder="First Name*" type="text" />
						</div>
						<div class="col-md-6">
							<input class="form-control mobile-padding-top" id="inf_field_LastName" name="inf_field_LastName" placeholder="Last Name*" type="text" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-6">
							<input class="form-control field-top-margin" id="inf_field_Company" name="inf_field_Company" placeholder="Organization*" type="text" />
						</div>
						<div class="col-md-6">
							<select class="form-control field-top-margin" id="inf_field_JobTitle" name="inf_field_JobTitle">
								<option disabled selected> -- Select Role -- </option>
								<option>C-Level</option>
								<option>VP</option>
								<option>Manager</option>
								<option>Sales Person</option>
								<option>Other</option>
							</select>
						</div><input type="hidden" name="emails[0].Label" value="Work" />
					</div>

					<div class="form-group">
						<div class="col-md-6">
							<input class="form-control field-top-margin" id="inf_field_Email" class="insightly_email" name="inf_field_Email" placeholder="Email*" type="text" />
						</div><input type="hidden" name="phones[0].Label" value="Work" />
						<div class="col-md-6">
							<input class="form-control field-top-margin" id="inf_field_Phone1" class="insightly_phone" name="inf_field_Phone1" placeholder="Phone*" type="text" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-12">
							<textarea class="form-control field-top-margin" rows="6" id="inf_custom_Message" name="inf_custom_Message" placeholder="Background Information"></textarea>
							<span>*These fields are required and must be valid.</span>
							<input type="hidden" id="websites[0]_Value" name="websites[0].Value" value="http://cardoneuniversity.com/#motivation"/>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-default">Get FREE Access</button>
				</div>
			</form>
		</div>
	</div>
</div>

<?php if ( is_singular( 'courses' ) ): ?>
	<?php global $post; ?>
	<!-- <?= $post->post_title ?> Modal -->
	<div class="modal fade" id="get-access-modal" tabindex="-1" role="dialog" aria-labelledby="get-access-modalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<img src="<?= get_stylesheet_directory_uri() . '/images/modal-header-getaccess.png' ?>">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="get-access-modalLabel">Complete the form below</h4>
				</div>
				<form id="get-access-modalForm" name="insightly_web_to_contact" action="<?= get_post_meta( $post->ID, 'wpcf-form-action', true ) ?>" method="post">
					<div class="modal-body">
						<input name="inf_form_xid" type="hidden" value="<?= substr( get_post_meta( $post->ID, 'wpcf-form-action', true ), strrpos( get_post_meta( $post->ID, 'wpcf-form-action', true ), '/' ) + 1 ) ?>" />
						<input name="inf_form_name" type="hidden" value="<?= get_post_meta( $post->ID, 'wpcf-is-form-name', true ) ?>" />
						<input name="infusionsoft_version" type="hidden" value="1.33.0.43" />

						<div class="form-group">
							<div class="col-md-6">
								<input class="form-control" id="inf_field_FirstName" name="inf_field_FirstName" placeholder="First Name*" type="text" />
							</div>
							<div class="col-md-6">
								<input class="form-control mobile-padding-top" id="inf_field_LastName" name="inf_field_LastName" placeholder="Last Name*" type="text" />
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6">
								<input class="form-control field-top-margin" id="inf_field_Company" name="inf_field_Company" placeholder="Organization*" type="text" />
							</div>
							<div class="col-md-6">
								<select class="form-control field-top-margin" id="inf_field_JobTitle" name="inf_field_JobTitle">
									<option disabled selected> -- Select Role -- </option>
									<option>C-Level</option>
									<option>VP</option>
									<option>Manager</option>
									<option>Sales Person</option>
									<option>Other</option>
								</select>
							</div><input type="hidden" name="emails[0].Label" value="Work" />
						</div>

						<div class="form-group">
							<div class="col-md-6">
								<input class="form-control field-top-margin" id="inf_field_Email" class="insightly_email" name="inf_field_Email" placeholder="Email*" type="text" />
							</div><input type="hidden" name="phones[0].Label" value="Work" />
							<div class="col-md-6">
								<input class="form-control field-top-margin" id="inf_field_Phone1" class="insightly_phone" name="inf_field_Phone1" placeholder="Phone*" type="text" />
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-12">
								<textarea class="form-control field-top-margin" rows="6" id="inf_custom_Message" name="inf_custom_Message" placeholder="Background Information"></textarea>
								<span>*These fields are required and must be valid.</span>
								<input type="hidden" id="websites[0]_Value" name="websites[0].Value" value="http://cardoneuniversity.com/#motivation"/>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-default">Get Access</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- Open modal with Get Access form afer 5 seconds. -->
	<script type="text/javascript">
		// jQuery(window).load(function(){
		// 	setTimeout(function(){ jQuery('#get-access-modal').modal('show'); }, 5000);
		// });
	</script>
<?php endif; ?>

<script type="text/javascript">
	var modals = jQuery('#get-access-modal, #full-cu-modal, #free-cu-modal');
	var modalForms = jQuery('#get-access-modalForm, #full-cu-modalForm, #free-cu-modalForm');

	// Open Free Access modal 
	jQuery( ".open-free-access-modal" ).click(function(ev) {
		jQuery("#free-cu-modal").modal("show"); 
		ev.preventDefault();
	});

</script>
<?php }
add_action( 'wp_footer', 'cu_hook_modals_to_footer' );


// Hooking up between <header></header> for Lead Forensics tracking script.
function cu_hook_lead_forensics_script() { ?>
<script type="text/javascript" src="http://www.glb12pkgr.com/js/68604.js" ></script>
<noscript><img src="http://www.glb12pkgr.com/68604.png" style="display:none;" /></noscript>
<?php }
add_action( 'wp_head', 'cu_hook_lead_forensics_script' );


// Add new crop size for Case Studies images
if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'case-study-thumb', 585, 585, true );
}

// Custom JS scripts
function cu_footer_custom_js() { ?>
<!-- LightSpeed counter -->
<script src="http://a.lightspeedvt.com/v1/clients/grant_cardone/counter/init.cfm"></script>

<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
	window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
		d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
			_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
			$.src="//v2.zopim.com/?2EDCV37tkJT1wjQS4wQew8KZF3SDwclQ";z.t=+new Date;$.
			type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
		</script>
		<!--End of Zopim Live Chat Script-->

		<!-- Go to www.addthis.com/dashboard to customize your tools -->
		<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5512dfd76e08a446"></script>
		<?php }
		add_action( 'wp_footer', 'cu_footer_custom_js' );

