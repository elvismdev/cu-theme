<?php
/* ------------------------------------------------------------------------ */
/* Theme Single Post
/* ------------------------------------------------------------------------ */
get_header(); ?>
<!--left col-->

<?php 
$pdfURL = types_render_field( "case-study-pdf", array( "output"=>"raw" ) );
$bannerImg = types_render_field( "case-study-banner", array( "output"=>"raw" ) );
?>

<div class="sd-blog-page">

	<div class="title-banner case-study-page-header" style="background-image: url(&quot;<?= $bannerImg; ?>&quot;); background-position: 50% 0%; background-size: 100% auto;" data-stellar-background-ratio=".1">
		<div class="row">
			<div class="columns-12">
				<div class="banner-outer">
					<div class="banner-inner">
						<div>
							<h1 style="opacity: 1;"><?php the_title(); ?></h1>
						</div>
						<?php if ( $pdfURL ): ?>
							<a class="button" href="<?= $pdfURL ?>" target="_blank" download>Download Case Study PDF</a>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row"> 
			<div class="col-md-12">
				<div id="single-case-study">
					<?php if (have_posts()) : while (have_posts()) : the_post();?>

						<?php get_template_part( 'templates/single', 'case-study' ); ?>

					<?php endwhile; else: ?>
					<p>
						<?php _e( 'Sorry, no posts matched your criteria', 'sd-framework' ) ?>
						.</p>
					<?php endif; ?>
					<?php if ( $sd_data['sd_blog_comments'] == '1' ) : ?>
						<!--comments-->
						<?php comments_template( '', true ); ?>
						<!--comments end--> 
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>
