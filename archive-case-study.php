<?php
/* ------------------------------------------------------------------------ */
/* Theme Archive for Case Studies
/* ------------------------------------------------------------------------ */
get_header();

?>

<div class="sd-blog-page case-studies-template">
	<div class="container">
		<div class="row"> 
			<div class="col-md-12">
				<div id="case-studies">
					<?php global $wp_query;
					global $more;
					$more = 0;
					?>
					<?php if ( have_posts() ) : ?>
						<ul class="block-grid-4 case-studies-isotope-grid">
							<?php while ( have_posts() ) : the_post(); ?>
								<?php get_template_part( 'templates/content', 'case-study' ); ?>
							<?php endwhile; ?> 
						</ul>
					<?php else: ?>
						<p>
							<?php _e( 'Sorry, no posts matched your criteria', 'sd-framework' ) ?>
							. </p>
						<?php endif; wp_reset_postdata(); ?>
						<!--pagination-->
						<?php if ( $sd_data['sd_pagination_type'] == '1' ) : ?>
							<?php if ( get_previous_posts_link() ) : ?>
								<div class="sd-nav-previous">
									<?php previous_posts_link( $sd_data['sd_blog_prev'] ); ?>
								</div>
							<?php endif; ?>
							<?php if ( get_next_posts_link() ) : ?>
								<div class="sd-nav-next">
									<?php next_posts_link( $sd_data['sd_blog_next'] ); ?>
								</div>
							<?php endif; ?>
						<?php else : sd_custom_pagination(); endif; ?>
							<!--pagination end--> 
							<!--pagination end-->
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php get_footer(); ?>
