<?php
/* ------------------------------------------------------------------------ */
/* Theme Footer
/* ------------------------------------------------------------------------ */
global $sd_data;
?>
<?php
$sd_newsletter = $sd_data['sd_newsletter_display'];
if ( $sd_newsletter !== '1' ) {
	if ( $sd_newsletter == '2' ) {
		if ( is_front_page() ) {
			get_template_part( 'framework/inc/newsletter' );
		}
	} else {
		get_template_part( 'framework/inc/newsletter' );
	}
}
?>

<footer id="sd-footer">
	<?php if ( $sd_data['sd_twitter_box'] == '1' ) : ?>
		<?php get_template_part( 'framework/inc/latest-tweets' ); ?>
	<?php endif; ?>
	
	
	<?php if ( $sd_data['sd_widgetized_footer'] == '1' ) : ?>
		<!-- footer widgets -->
		<div class="sd-footer-widgets">
			<div class="container">
				<div class="row">
					<!-- Footer Logos 
					<div class="col-md-12 col-sm-12 text-center">
						<img class="fixed-ratio-resize" src="<?php //echo get_stylesheet_directory_uri(); ?>/images/footer-logos.jpg">
					</div> -->
					<div class="col-md-4 col-sm-6">
						<?php dynamic_sidebar( 'footer-left-sidebar' ); ?>
					</div>
					<div class="col-md-4 col-sm-6">
						<?php dynamic_sidebar( 'footer-middle-sidebar' ); ?>
					</div>
					<div class="col-md-4 col-sm-6">
						<?php dynamic_sidebar( 'footer-right-sidebar' ); ?>
					</div>
				</div>
			</div>
		</div>
		<!-- footer widgets end -->
	<?php endif; ?>
	
	<?php if ( $sd_data['sd_copyright'] == '1' ) : ?>
		<div class="sd-copyright">
			<div class="container">
				<?php if ( !empty( $sd_data['sd_copyright_text'] ) ) : ?>
					<?php echo do_shortcode( $sd_data['sd_copyright_text'] ) ;?>
				<?php else : ?>
					<span class="sd-copyright-text">Copyright &copy; <?php echo date( 'Y' ); ?> - <a href="http://skat.tf" title="Premium WordPress Themes" rel="home"> Skat Design </a></span>
				<?php endif; ?>
			</div>
		</div>
	<?php endif; ?>

	<div id="footer-gc-properties-logos">
		<div class="container">
			<div class="row">
				<div class="col-xs-6 col-sm-3">
					<a target="_blank" href="http://www.grantcardone.com/"><img class="property-logo" src="<?php echo get_stylesheet_directory_uri(); ?>/images/GrantCardone.png"></a>
				</div>
				<div class="col-xs-6 col-sm-3">
					<a target="_blank" href="http://cardoneondemand.com/"><img class="property-logo" src="<?php echo get_stylesheet_directory_uri(); ?>/images/CardoneOnDemand.png"></a>
				</div>
				<div class="col-xs-6 col-sm-3">
					<a target="_blank" href="http://grantcardonetv.com/"><img class="property-logo" src="<?php echo get_stylesheet_directory_uri(); ?>/images/GrantCardoneTV-Logo.png"></a>
				</div>
				<div class="col-xs-6 col-sm-3">
					<a target="_blank" href="http://cardoneacquisitions.com/"><img class="property-logo" src="<?php echo get_stylesheet_directory_uri(); ?>/images/CardoneAcquisitions-logo.png"></a>
				</div>
			</div>
		</div>
	</div>

</footer>
<!-- footer end -->

<?php wp_footer(); ?>

</body>
</html>