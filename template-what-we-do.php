<?php 
/* ------------------------------------------------------------------------ */
/* TTemplate Name: Page: What We Do
/* ------------------------------------------------------------------------ */
get_header(); 
?>
<!--left col-->

<div class="sd-blog-page">
	<div class="container">
		<div class="row"> 
			<!--left col-->
			<div class="col-md-12 <?php if ( $sd_data['sd_sidebar_location'] == '2' ) echo 'pull-right'; ?>">
				<div class="sd-left-col">
					<?php if (have_posts()) : while (have_posts()) : the_post();?>
						<article id="post-<?php the_ID(); ?>" <?php post_class( 'blog-entry page-entry clearfix' ); ?>> 

							<!-- entry content -->
							<div class="entry-content">
								<?php the_content(); ?>
								<?php wp_link_pages( 'before=<strong class="page-navigation clearfix">&after=</strong>' ); ?>
							</div>
							<!-- entry content end--> 
						</article>
						<!--post-end-->

					<?php endwhile; else: ?>
					<p>
						<?php _e( 'Sorry, no posts matched your criteria', 'sd-framework' ) ?>
						.</p>
					<?php endif; ?>
				</div>
			</div>
			<!--left col end--> 
			<!--sidebar-->
			<!-- <div class="col-md-4">
				<?php //get_sidebar(); ?>
			</div> -->
			<!--sidebar end--> 
		</div>
		<!-- <div class="vc_separator wpb_content_element vc_el_width_100 vc_sep_color_custom row-below-desc" style="margin-bottom: 35px; padding-top: 20px;"></div>
		<div class="row">
			<div class="col-md-12">
				<div class="modules-menu">
					<?php //wp_nav_menu( array( 'menu' => 'Module Details Menu' ) ); ?>
				</div>
			</div>
		</div>
		<div class="vc_separator wpb_content_element vc_el_width_100 vc_sep_color_custom row-below-desc" style="margin-bottom: 35px; padding-top: 20px;"></div>
		<div class="row">
			<div class="col-md-12">
				<a id="buy-full-cu-link" href="#" data-toggle="modal" data-target="#full-cu-modal"><img src="/wp-content/uploads/2015/07/full-package.jpg"/></a>
			</div>
		</div> -->
	</div>
</div>

<?php get_footer(); ?>
