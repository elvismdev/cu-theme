<?php 
/* ------------------------------------------------------------------------ */
/* Template Name: Page: Testimonials V2
/* ------------------------------------------------------------------------ */
get_header(); 
?>

<div class="sd-blog-page testimonials-v2">
	<div class="container">
		<div class="row"> 
			<div class="col-md-12">

				<div id="testimonials-v2">

					<?php
					global $wp_query;

					$paged = get_query_var( 'paged ') ? get_query_var( 'paged' ) : 1;
					$args = array(
						'post_type' 		=> 'testimonials',
						'posts_per_page' 	=> 100,
						'post_status' 		=> 'publish',
						'paged' 			=> $paged
						);

					$wp_query = new WP_Query( $args );

					while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>

					<?php
					$comp_name = types_render_field( "company-name", array( "output"=>"raw" ) );
					$comp_website = types_render_field( "company-website", array( "output"=>"raw" ) );
					$twitter_handle = types_render_field( "twitter-handle", array( "output"=>"raw" ) );
					?>

					<div class="testimonials-list group">
						<div class="thumb-testimonial group">
							<?php if ( ( function_exists( 'has_post_thumbnail' ) ) && ( has_post_thumbnail() ) ) : ?>
								<div class="sphere"><?php the_post_thumbnail(); ?></div>
							<?php endif; ?>
						</div>
						<div class="the-post group">
							<?php the_content(); ?>
							<p class="name-testimonial group">
								<span class="title special-font"><?php the_title(); ?></span> 
								<?php if ( $twitter_handle ) : ?>
									<span class="separator"></span><span class="twitter"><a target="_blank" href="https://twitter.com/<?php printf( "%s", $twitter_handle ); ?>">@<?php printf( "%s", $twitter_handle ); ?></a></span>
								<?php endif; ?>
								<?php if ( $comp_name ) : ?>
									<span class="separator"></span><span class="website"><?php if ( $comp_website ) : ?><a target="_blank" href="<?php printf( "%s", $comp_website ); ?>"><?php endif; ?><?php printf( "%s", $comp_name ); ?></a></span>
								<?php endif; ?>
							</p>
						</div>
					</div>

				<?php endwhile; ?>

			</div>

			<!--pagination-->
			<?php sd_custom_pagination();  ?>
			<!--pagination end--> 

		</div>
	</div>
</div>
</div>

<?php get_footer(); ?>
