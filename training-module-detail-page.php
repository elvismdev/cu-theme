<?php 
/* ------------------------------------------------------------------------ */
/* Template Name: Page: Training Module Detail Page
/* ------------------------------------------------------------------------ */
get_header(); 
?>

<div class="sd-blog-page">
	<div class="container">
		<div class="row"> 
			<div class="col-md-12 <?php if ( $sd_data['sd_sidebar_location'] == '2' ) echo 'pull-right'; ?>">
				<div class="sd-left-col">
					<?php if (have_posts()) : while (have_posts()) : the_post();?>
						<article id="post-<?php the_ID(); ?>" <?php post_class( 'blog-entry page-entry clearfix' ); ?>> 

							<!-- entry content -->
							<div class="entry-content">
								<?php the_content(); ?>
								<?php wp_link_pages( 'before=<strong class="page-navigation clearfix">&after=</strong>' ); ?>
							</div>
							<!-- entry content end--> 
						</article>
						<!--post-end-->

					<?php endwhile; else: ?>
					<p>
						<?php _e( 'Sorry, no posts matched your criteria', 'sd-framework' ) ?>
						.</p>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Buy Now Modal -->
<div class="modal fade" id="buy-now-modal" tabindex="-1" role="dialog" aria-labelledby="buy-now-modalLabel" style="display: none;">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="buy-now-modalLabel">Complete the form below to get <?= ( types_render_field( 'free-module', array( 'output' => 'raw' ) ) == 1 ) ? 'FREE' : '' ?> Access</h4>
			</div>
			<form id="buy-now-modalForm" name="insightly_web_to_contact" action="<?= ( types_render_field( 'form-action', array( 'output' => 'raw' ) ) ) ? types_render_field( 'form-action', array( 'output' => 'raw' ) ) : '' ?>" method="post">
				<div class="modal-body">
					<input name="inf_form_xid" type="hidden" value="3e17c653e0980a5a54cc77c64b11938b" />
					<input name="inf_form_name" type="hidden" value="CU A La Carte" />
					<input name="infusionsoft_version" type="hidden" value="1.33.0.43" />

					<div class="form-group">
						<div class="col-md-6">
							<input class="form-control" id="inf_field_FirstName" name="inf_field_FirstName" placeholder="First Name*" type="text" />
						</div>
						<div class="col-md-6">
							<input class="form-control" id="inf_field_LastName" name="inf_field_LastName" placeholder="Last Name*" type="text" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-6">
							<input class="form-control field-top-margin" id="inf_field_Company" name="inf_field_Company" placeholder="Organization*" type="text" />
						</div>
						<div class="col-md-6">
							<select class="form-control field-top-margin" id="inf_field_JobTitle" name="inf_field_JobTitle">
								<option disabled selected> -- Select Role -- </option>
								<option>C-Level</option>
								<option>VP</option>
								<option>Manager</option>
								<option>Sales Person</option>
								<option>Other</option>
							</select>
						</div><input type="hidden" name="emails[0].Label" value="Work" />
					</div>

					<div class="form-group">
						<div class="col-md-6">
							<input class="form-control field-top-margin" id="inf_field_Email" class="insightly_email" name="inf_field_Email" placeholder="Email*" type="text" />
						</div><input type="hidden" name="phones[0].Label" value="Work" />
						<div class="col-md-6">
							<input class="form-control field-top-margin" id="inf_field_Phone1" class="insightly_phone" name="inf_field_Phone1" placeholder="Phone*" type="text" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-12">
							<textarea class="form-control field-top-margin" rows="6" id="inf_custom_Message" name="inf_custom_Message" placeholder="Background Information"></textarea>
							<span>*These fields are required and must be valid.</span>
							<input type="hidden" id="websites[0]_Value" name="websites[0].Value" value="http://cardoneuniversity.com/#motivation"/>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-default">Get <?= ( types_render_field( 'free-module', array( 'output' => 'raw' ) ) == 1 ) ? 'FREE' : '' ?> Access</button>
				</div>
			</form>
		</div>
	</div>
</div>


<?php get_footer(); ?>
