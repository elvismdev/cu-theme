<?php
/* ------------------------------------------------------------------------ */
/* Theme Single Post - Standard Post Format
/* ------------------------------------------------------------------------ */
global $sd_data;
$sd_data['sd_blog_single_prev'] = "Previous Case";
$sd_data['sd_blog_single_next'] = "Next Case";
?>

<?php
$objectives = types_render_field( "objectives", array( "separator"=>"</span></li><li><span>" ) );
$solution = types_render_field( "solution", array( "separator"=>"</span></li><li><span>" ) );
$results = types_render_field( "results", array( "separator"=>"</span></li><li><span>" ) );
$clientPic = types_render_field( "client-picture", array() );
$challenge = types_render_field( "challenge", array() );
$approach = types_render_field( "approach", array() );
$resultsDetails = types_render_field( "results-details", array() );
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'sd-blog-entry sd-single-blog-entry clearfix' ); ?>> 
	<!-- entry wrapper -->
	<div class="sd-entry-wrapper">
		

		

		<!-- entry content  -->
		<div class="sd-entry-content about-case-study row">

			<div class="columns-8 column-center">
				<h5>About The Company</h5>

				<?php if ( $clientPic ) echo $clientPic; ?>

				<?php the_content(); ?>
			</div>

			<?php wp_link_pages( 'before=<strong class="sd-page-navigation clearfix">&after=</strong>' ); ?>
		</div>

		<div class="obj-req-wrapper">
			<section class="row">
				<div class="col-md-4 objectives">
					<?php if ( $objectives ): ?>
						<h5>Objectives</h5>
						<ul>
							<li><span><?php echo $objectives; ?></span></li>
						</ul>
					<?php endif; ?>
				</div>

				<div class="col-md-4 solutions">
					<?php if ( $solution ): ?>
						<h5>Solution</h5>
						<ul>
							<li><span><?php echo $solution; ?></span></li> 
						</ul>
					<?php endif; ?>
				</div>

				<div class="col-md-4 results">
					<?php if ( $solution ): ?>
						<h5>Results</h5>
						<ul>
							<li><span><?php echo $results; ?></span></li> 
						</ul>
					<?php endif; ?>
				</div>
			</section>
		</div>

		<div class="obj-req-wrapper outcomes-details">

			<?php if ( $challenge ): ?>
				<section class="row">
					<div class="columns-8 column-center">
						<h5>Challenge</h5>
						<?php echo $challenge; ?>
					</div>
				</section>
				<div class="separator"></div>
			<?php endif; ?>

			<?php if ( $approach ): ?>
				<section class="row">
					<div class="columns-8 column-center">
						<h5>Approach</h5>
						<?php echo $approach; ?>
					</div>
				</section>
				<div class="separator"></div>
			<?php endif; ?>

			<?php if ( $resultsDetails ): ?>
				<section class="row">
					<div class="columns-8 column-center">
						<h5>Results</h5>
						<?php echo $resultsDetails; ?>
					</div>
				</section>
				<div class="separator"></div>
			<?php endif; ?>

		</div>

		<!-- <div class="share-btns">

			<section class="row">
				<div class="columns-8 column-center">
					<h5>Share This</h5>

					<?php //do_action( 'addthis_widget', get_permalink(), get_the_title(), array(
						//'type' => 'custom',
						//'size' => '32',
						//'services' => 'facebook,twitter,google_plusone_share,linkedin,email',
						//'more' => false, 
						//'counter' => false
						//));
						?>
					</div>
				</section>

			</div> -->

			<!-- entry content end -->
			<?php if ( $sd_data['sd_blog_single_prev_next'] == '1' ) : ?>
				<footer>
					<div class="sd-prev-next-post clearfix">
						<span class="sd-prev-post">	<?php next_post_link( '%link', '&larr; ' . $sd_data['sd_blog_single_prev'] ); ?> </span>
						<span class="sd-next-post">	<?php previous_post_link( '%link',  $sd_data['sd_blog_single_next'] . ' &rarr;' ); ?> </span>
					</div>
				</footer>
			<?php endif; ?>
		</div>
		<!-- entry wrapper end--> 
	</article>
<!--post-end-->