<?php
/* ------------------------------------------------------------------------ */
/* Theme Index Content - Case Study Format
/* ------------------------------------------------------------------------ */
global $sd_data;
?>

<li id="post-<?php the_ID(); ?>" <?php post_class( 'sd-blog-entry sd-standard-entry clearfix' ); ?>>

	<?php if ( $sd_data['sd_blog_featured_img'] == '1' ) : ?>
		<!-- post thumbnail -->
		<?php if ( ( function_exists( 'has_post_thumbnail') ) && ( has_post_thumbnail() ) ) : ?>
			<?php the_post_thumbnail( 'case-study-thumb' ); ?>
		<?php endif; ?>
		<!-- post thumbnail end--> 
	<?php endif; ?>

	<div class="case-study-meta">

		<h3><?php the_title(); ?></h3>

		<a class="button" href="<?php the_permalink(); ?>">View Case Study</a>

		<?php if ( has_term( '', 'industry' ) ) : ?>
			<?php $industries = get_the_terms( $post->ID, 'industry' ); ?>
			<ul class="case-industries">
				<?php foreach ( $industries as $industry ): ?>
					<li class="filter-trigger" data-filter="industries" data-key="<?php echo $industry->slug; ?>"><?php echo $industry->name; ?></li>
				<?php endforeach; ?>
			</ul>
		<?php endif; ?>

		<p><?php echo $post->post_excerpt; ?></p>

	</div> 

</li>
<!--post-end--> 