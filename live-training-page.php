<?php
/* ------------------------------------------------------------------------ */
/* Template Name: Page: Live Training
/* ------------------------------------------------------------------------ */
get_header();
?>

<div class="sd-blog-page">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-8 <?php if ( $sd_data['sd_sidebar_location'] == '2' ) echo 'pull-right'; ?>">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class( 'sd-full-width clearfix' ); ?>>

						<!-- entry content -->
						<div class="entry-content">
							<?php the_content(); ?>
						</div>
						<!-- entry content end-->
					</article>
					<!--post-end-->

				<?php endwhile; else: ?>
				<p>
					<?php _e( 'Sorry, no posts matched your criteria', 'sd-framework' ) ?>
					.</p>
				<?php endif; ?>
				<!-- content end -->
			</div>
			<div class="col-md-4">
				<?php if ( ! post_password_required() ) : ?>
					<?php if ( $sd_data['sd_blog_comments'] == '1' ) : ?>
						<!--comments-->
						<?php comments_template( '', true ); ?>
						<!--comments end-->
					<?php endif; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>